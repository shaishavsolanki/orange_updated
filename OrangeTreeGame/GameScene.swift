//
//  GameScene.swift
//  OrangeTreeGame
//
//  Created by MacStudent on 2019-02-20.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import SpriteKit
import GameplayKit



class GameScene: SKScene,SKPhysicsContactDelegate{
    
    var orange:Orange?
    var statingPositionOfOrange:CGPoint = CGPoint(x: 0, y: 0)
    var lineNode = SKShapeNode()
    
    
    
    override func didMove(to view: SKView) {
        
        //add boundary around the scene
        
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.name = "wall"
        //initialize delegate
        self.physicsWorld.contactDelegate = self
        
        //confuguration to draw a line
        
        self.lineNode.lineWidth = 20
        self.lineNode.lineCap = .round
        self.lineNode.strokeColor = UIColor.black
        addChild(lineNode)
        
    
        
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        //detect the touch
        
        guard let touch = touches.first else{
            return
        }
        
        //get the location where user touched the screen
        let mouseLocation = touch.location(in: self)
        
        print("Finger Starting position is at: \(mouseLocation)")
        
        
        //detect what sprite was touch
        
        let spriteTouch = self.atPoint(mouseLocation)
        
        if(spriteTouch.name == "tree"){
           // print("Clicked on the tree")
            self.orange = Orange()
            orange?.position = mouseLocation
            addChild(orange!)
            self.orange?.physicsBody?.restitution = 1.0
            
        self.orange?.physicsBody?.isDynamic = false
            
            //set the starting position of the finger
            self.statingPositionOfOrange = mouseLocation
            
            
        }
        else{
            //print("touched somewhere on screen: \(spriteTouch.name)")
        }
        //add orange when screen touched
        
       
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else{
            return
        }
        
        //get the location where user touched the screen
        let mouseLocation = touch.location(in: self)
        
        print("Finger Ending position is at: \(mouseLocation)")
        
        let orangeEndingPosition = mouseLocation
        
        
        //get the difference between finger start and end
        let diffX = orangeEndingPosition.x - self.statingPositionOfOrange.x
        
        let diffY = orangeEndingPosition.y - self.statingPositionOfOrange.y
        
       
        
        //draw a line where orange is thrown
        let path = UIBezierPath()
        path.move(to: self.statingPositionOfOrange)
        path.addLine(to: mouseLocation)
        
        self.lineNode.path = nil
        
        
        
        // throw the orange based on that direction
        
        let direction = CGVector(dx: diffX, dy: diffY)
         self.orange?.physicsBody?.isDynamic = true
        self.orange?.physicsBody?.applyImpulse(direction)

      
 
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
     
        guard let touch = touches.first else{
            return
        }
        
        //get the location where user touched the screen
        let mouseLocation = touch.location(in: self)
        
        //update the location of the mouse
        self.orange?.position = mouseLocation
        
        
        //draw a line
        let path = UIBezierPath()
        path.move(to:self.statingPositionOfOrange)
        path.addLine(to:mouseLocation)
        self.lineNode.path = path.cgPath

    }
    func didBegin(_ contact: SKPhysicsContact) {
        
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        
        if(contact.collisionImpulse > 15){
            
            if(nodeA?.name == "skull"){
                print("Skull hits : \(nodeB?.name)")
                print("Collision : \(contact.collisionImpulse)")
                print("----")
                
                let reduceImageSize = SKAction.scale(by: 0.8, duration: 0.5)
                let removeNode = SKAction.removeFromParent()
                
                let seq = SKAction.sequence([reduceImageSize,removeNode])
                
                nodeA?.run(seq)
                self.gameOver()
            }else if(nodeA?.name == "skull"){
                print("Skull hits : \(nodeB?.name)")
                print("Collision : \(contact.collisionImpulse)")
                print("----")
                let reduceImageSize = SKAction.scale(by: 0.8, duration: 0.5)
                let removeNode = SKAction.removeFromParent()
                
                let seq = SKAction.sequence([reduceImageSize,removeNode])
                
               nodeB?.run(seq)
                self.gameOver()
                
            }
            
        }
        
        
    }
    

    func gameOver(){
     
        
        let message = SKLabelNode(text:"YOU Win!")
        message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        message.fontColor = UIColor.magenta
        message.fontSize = 100
        message.fontName = "AvenirNext-Bold"
        
        addChild(message)
        
        // restart the game after 3 seconds
        perform(#selector(GameScene.restartGame), with: nil,
                afterDelay: 3)
        

    }
    
    @objc func restartGame() {
        let scene = GameScene(fileNamed:"GameScene")
        scene!.scaleMode = scaleMode
        view!.presentScene(scene)
        //for Commit reasons
        //add Restart And collision effcts
    }
    


}
